using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaAreaPuntaje : MonoBehaviour
{
    private AudioSource moneda;


    // Start is called before the first frame update
    void Start()
    {
        moneda = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        LogicaPuntaje.puntaje++;
        moneda.Play();
    }
}
