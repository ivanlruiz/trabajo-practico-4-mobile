using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Jugador : MonoBehaviour
{
    public float velocity = 1; //velocidad del jugador
    private Rigidbody2D rb;
    private AudioSource Salto;


    public ControladorEscena controladorEscena;



    // Start is called before the first frame update
    void Start()
    {
        Salto = GetComponent<AudioSource>();

        rb = GetComponent<Rigidbody2D>(); //RigidBody del personaje
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            rb.velocity = Vector2.up * velocity; //Velocidad del personaje al apretar
            Salto.Play();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision) //Funcion para que detecte si el personaje choc� con algo
    {
        SceneManager.LoadScene(5);

    }

}
