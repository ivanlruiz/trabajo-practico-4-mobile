using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bala : MonoBehaviour
{
    public GameObject canvasPerdiste;
    private PuntajeJuego4 Puntaje;

    private void Start()
    {
        Puntaje = FindObjectOfType<PuntajeJuego4>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Esfera"))
        {
            transform.SetParent(collision.transform);
            Puntaje.puntaje++;
            Puntaje.ActualizarPuntaje();
        }

        if (collision.gameObject.CompareTag("Bala"))
        {
            SceneManager.LoadScene(5);
        }
    }
}
