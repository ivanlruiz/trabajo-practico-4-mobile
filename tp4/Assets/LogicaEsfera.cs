using UnityEngine;

public class LogicaEsfera : MonoBehaviour
{
    public float velocidadRotacion = 10f; // Velocidad de rotaci�n ajustable

    void Update()
    {
        // Rotar el objeto en el eje Z con la velocidad especificada
        transform.Rotate(0f, 0f, velocidadRotacion * Time.deltaTime);
    }
}
