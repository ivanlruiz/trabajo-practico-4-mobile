using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPerdiste : MonoBehaviour
{
    public void Retry ()
    {
        Application.Quit ();
    }

    public void Menu()
    {
        SceneManager.LoadScene(0);
    }
}
