using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public GameObject canvasPerdiste;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala") && !collision.gameObject.CompareTag("Bala"))
        {
            canvasPerdiste.gameObject.SetActive(true);
        }

        if (collision.gameObject.CompareTag("Bala") && gameObject.CompareTag("Esfera"))

        {
            collision.transform.SetParent(transform);
        }
    }
}
