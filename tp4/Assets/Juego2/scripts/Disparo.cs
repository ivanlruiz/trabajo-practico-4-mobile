using UnityEngine;

public class Disparo : MonoBehaviour
{
    public GameObject prefabBala; // Prefab de la bala a disparar
    public float velocidadBala = 10f; // Velocidad de la bala ajustable desde el inspector

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {           
            GameObject bala = Instantiate(prefabBala, transform.position, transform.rotation);           
            Rigidbody rb = bala.GetComponent<Rigidbody>();
            rb.AddForce(Vector3.right * velocidadBala, ForceMode.Impulse);
        }
    }
}



