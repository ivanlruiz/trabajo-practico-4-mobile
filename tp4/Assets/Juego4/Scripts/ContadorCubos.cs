using UnityEngine;
using TMPro;

public class ContadorCubos : MonoBehaviour
{
    public TextMeshProUGUI textoNaranjas;
    public TextMeshProUGUI textoAzules;

    private int cantidadNaranjas = 0;
    private int cantidadAzules = 0;

    private void Start()
    {
        ActualizarContador();
    }

    public void ActualizarContador()
    {
        // Obtener todos los objetos con el tag "CuboNaranja" y contarlos
        GameObject[] cubosNaranjas = GameObject.FindGameObjectsWithTag("CuboNaranja");
        cantidadNaranjas = cubosNaranjas.Length;

        // Obtener todos los objetos con el tag "CuboAzul" y contarlos
        GameObject[] cubosAzules = GameObject.FindGameObjectsWithTag("BlueCube");
        cantidadAzules = cubosAzules.Length;

        // Actualizar los textos con la cantidad de cubos naranjas y azules
        textoNaranjas.text = "" + cantidadNaranjas;
        textoAzules.text = "" + cantidadAzules;
    }

    private void Update()
    {
        // Actualizar el contador constantemente
        ActualizarContador();
    }
}
