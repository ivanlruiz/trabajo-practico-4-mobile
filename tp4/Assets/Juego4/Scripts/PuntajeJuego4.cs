using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PuntajeJuego4 : MonoBehaviour
{
    public TextMeshPro textMeshPro;
    public TextMeshProUGUI Textpuntaje;
    public int puntaje = 0;

    private void Start()
    {
        ActualizarPuntaje();
    }

    public void SumarPuntaje(int cantidad)
    {
        puntaje += cantidad;
        ActualizarPuntaje();
    }

    private void Update()
    {
        if (puntaje == 20)
        {
            textMeshPro.gameObject.SetActive(true);
        }
    }
    public void ActualizarPuntaje()
    {
        Textpuntaje.text = puntaje.ToString();
    }
}
