using UnityEngine;

public class LogicaCubo : MonoBehaviour
{
    public Material blueMaterial;
    public Material orangeMaterial;

    public float forceMagnitude = 5f;

    private Rigidbody rb;
    private bool isBlue;
    private ContadorCubos contadorCubos;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        isBlue = CompareTag("BlueCube");
        contadorCubos = FindObjectOfType<ContadorCubos>();

        if (isBlue)
        {
            rb.useGravity = false;
        }
        else
        {
            rb.useGravity = true;
            rb.velocity = Vector3.down;
        }
    }
    private void FixedUpdate()
    {
        if (isBlue)
        {
            rb.AddForce(Vector3.up * forceMagnitude, ForceMode.Force);
        }
        else
        {
            rb.AddForce(Vector3.down * forceMagnitude, ForceMode.Force);
        }
    }
    private void OnMouseDown()
    {
        ChangeColorAndGravity();
        contadorCubos.ActualizarContador();
    }
    private void ChangeColorAndGravity()
    {
        if (isBlue)
        {
            GetComponent<Renderer>().material = orangeMaterial;
            rb.velocity = Vector3.down;
            isBlue = false;
            tag = "CuboNaranja";
        }
        else
        {
            GetComponent<Renderer>().material = blueMaterial;
            rb.useGravity = false;
            isBlue = true;
            tag = "BlueCube";
        }
    }
}
